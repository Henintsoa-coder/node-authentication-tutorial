if (process.env.NODE_ENV !== 'prod') {
    require('dotenv').config();    
}

console.log('Console running !!!');
const express = require('express');
const app = express();
const bcrypt = require('bcrypt');
const initializePassport = require('./passport-config.js');
const passport = require('passport');
const flash = require('express-flash');
const session = require('express-session');
const methodOverRide = require('method-override');
const checkAuthenticated = (req, res, next) =>
{
    if (req.isAuthenticated()) {
        return next();
    }

    res.redirect('/login');
}

const checkNotAuthenticated = (req, res, next) => {
    if (req.isAuthenticated()) {
        return res.redirect('/');
    }

    next();
};

initializePassport(
    passport, 
    email => users.find(user => user.email === email),
    id => users.find(user => user.id === id)
);

app.set('view-engine', 'ejs');
app.use(express.urlencoded({extended:false}));
app.use(flash());
app.use(session({
    secret:process.env.SECRET_SESSION,
    resave:false,
    saveUninitialized:false
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(methodOverRide('_method'));

const users = [];

/**
 * Home
 */
app.get('/', checkAuthenticated , (req, res) => {
    res.render('index.ejs', { name:req.user.name });
});

/**
 * Login
 */
 app.get('/login', checkNotAuthenticated, (req, res) => {
    res.render('login.ejs');
})

app.post('/login', checkNotAuthenticated, passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash:true
}))

/**
 * Logout
 */
app.delete('/logout', (req, res)=>{
    req.logout();
    res.redirect('/login');
});

/**
 * Register
 */
app.get('/register', checkNotAuthenticated, async (req, res) => {
    res.render('register.ejs');
})

app.post('/register', checkNotAuthenticated, async (req, res) => {
    try {
        const hashedPassword = await bcrypt.hash(req.body.password, 10);

        users.push({
            id: Date.now().toString(),
            name: req.body.name,
            email: req.body.email,
            password: hashedPassword
        });


        res.redirect('/login');
    } catch (error) {
        console.log(error);
        res.redirect('/register');
    }

    console.log(users);
})


app.listen(3000);